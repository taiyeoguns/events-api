import pytest

from app import create_app
from config import TestingConfig


@pytest.fixture(name="application")
def fixture_application():
    """Fixture to set up application with configuration

    Yields:
        application -- Application context
    """
    app = create_app(TestingConfig)
    flaskapp = app.app

    with flaskapp.app_context(), flaskapp.test_request_context():
        yield flaskapp


@pytest.fixture(name="client")
def fixture_client(application):
    """Fixture for HTTP test client

    Arguments:
        application -- Application context

    Yields:
        client -- HTTP client
    """
    with application.test_client() as c:
        yield c
