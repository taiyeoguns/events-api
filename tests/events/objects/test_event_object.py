from events.objects import Event
from datetime import datetime


def test_event_attributes():

    evt = Event("foo", "192.168.0.1", "2008-04-05 14:32:08")

    assert getattr(evt, "name") == "foo"
    assert getattr(evt, "source_ip", None) is not None
    assert isinstance(evt.added_at, datetime)
