from unittest.mock import MagicMock

import requests

from events.services import APIService

API_RESULT = {
    "city": "Minneapolis",
    "country": {"iso_code": "US", "name": "United States"},
    "location": {
        "accuracy_radius": 20,
        "latitude": 45.04,
        "longitude": -93.4865,
        "metro_code": 613,
        "time_zone": "America/Chicago",
    },
}


def test_get_ip_details(monkeypatch):
    mock_response = MagicMock()
    mock_response.return_value.json.return_value = API_RESULT

    monkeypatch.setattr(requests, "get", mock_response)

    ip = "1.1.1.1"

    api_service = APIService("api-url")

    assert api_service.get_ip_details(ip) == API_RESULT
    mock_response.assert_called_once()
