from unittest.mock import MagicMock

import pytest

from events.controllers import EventController
from events.models import Country, Event, Location


@pytest.fixture(name="event")
def fixture_event():
    _event = Event(name="Event 1", source_ip="1.0.0.1", city="City")

    _country = Country(name="Country", iso_code="CT")
    _location = Location(
        accuracy_radius=1, latitude=2.2, longitude=3.3, metro_code=4, time_zone="T/Z"
    )

    _event.country = _country
    _event.location = _location

    yield _event


def test_documentation_page(client):
    response = client.get("/api/ui", follow_redirects=True)
    assert response.status_code == 200


def test_get_event_by_ip(client, event, monkeypatch):

    mock_process_event = MagicMock()
    mock_process_event.return_value = event.to_mongo().to_dict()

    monkeypatch.setattr(EventController, "process_event", mock_process_event)

    response = client.get("/api/geolocate/128.101.101.101")

    assert response.status_code == 200
