import connexion
from app.converters import IPAddressConverter
from app.extensions import cache, db
from app.handlers import not_found
from config import Config
from events import events_bp


def create_app(cfg=Config):
    # create connexion app
    app = connexion.FlaskApp(__name__, specification_dir="../")

    # get flask app object
    flaskapp = app.app

    # load config
    flaskapp.config.from_object(cfg)

    # initialize db
    db.init_app(flaskapp)

    # initialize cache
    cache.init_app(flaskapp)

    # register url converter for ip address
    flaskapp.url_map.converters["ip"] = IPAddressConverter

    # register handlers
    flaskapp.register_error_handler(404, not_found)

    # register blueprints
    flaskapp.register_blueprint(events_bp)

    # add api spec to connexion
    app.add_api("openapi.yml")

    return app


flaskapp = create_app().app  # for flask run
