from flask_caching import Cache
from flask_mongoengine import MongoEngine

# set up cache
cache = Cache()

# set up mongo
db = MongoEngine()
