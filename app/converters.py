import ipaddress
from werkzeug.routing import BaseConverter, ValidationError


class IPAddressConverter(BaseConverter):
    """Converter to validate valid IP address provided in URL

    Arguments:
        BaseConverter {class} -- BaseConverterclass

    Raises:
        ValidationError: Exception for invalid value

    Returns:
        str -- Valid value for IP address
    """

    def to_python(self, value):
        try:
            return str(ipaddress.ip_address(value))
        except ValueError as e:
            raise ValidationError(e)

    def to_url(self, value):
        return str(ipaddress.ip_address(value))
