import ipaddress
from datetime import datetime


class Event:
    def __init__(self, name, source_ip, added_at, **kwargs):
        self.name = name
        self.source_ip = source_ip
        self.added_at = added_at
        self.other = kwargs

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, n):
        if not n or len(n) < 3:
            raise ValueError("Event name must contain at least 3 characters")
        self._name = n

    @property
    def source_ip(self):
        return self._source_ip

    @source_ip.setter
    def source_ip(self, sip):
        try:
            self._source_ip = ipaddress.ip_address(sip)
        except ValueError:
            raise

    @property
    def added_at(self):
        return self._added_at

    @added_at.setter
    def added_at(self, aa):
        try:
            self._added_at = datetime.strptime(aa, "%Y-%m-%d %H:%M:%S")
        except (ValueError, TypeError) as e:
            raise ValueError(f"Invalid date value: {e}")

    def __repr__(self):
        return f"<Event: {self.name}>"
