from flask import abort, jsonify, redirect, request

from app.extensions import cache
from events import events_bp
from events.controllers import EventController


@events_bp.route("/", methods=["GET"])
def index():
    return redirect("/api/ui")


@events_bp.route("/api/geolocate/<ip:ip_addr>", methods=["GET"])
@cache.cached(timeout=300)
def geolocate(ip_addr):
    """Get details from IP address

    Arguments:
        ip_addr {str} -- IP address as string

    Returns:
        dict -- Dictionary with IP details if found
    """
    return EventController.process_event(ip_addr)


@events_bp.route("/api/events/added/", methods=["GET"])
@cache.cached(timeout=120)
def events_between_dates():
    """Get events within provided date range

    Query arguments:
    from (required) - date string in format DD-MM-YYYY
    to (required) - date string in format DD-MM-YYYY

    Returns:
        list -- List of dictionaries with events found
    """
    if not set(["from", "to"]).issubset(set(request.args)):
        abort(400, "'from' and 'to' query arguments are required")

    from_date = request.args.get("from")
    to_date = request.args.get("to")

    events = EventController.process_events_between_dates(from_date, to_date)
    return jsonify(events)


@events_bp.route("/api/events/around/", methods=["GET"])
@cache.cached(timeout=300)
def events_around_coordinates():
    """Get events around provided coordinates

    Query arguments:
    latitude (required) - Latitude of coordinate
    longitude (required) - Longitude of coordinate

    Returns:
        list -- List of dictionaries with events found
    """
    if not set(["latitude", "longitude"]).issubset(set(request.args)):
        abort(400, "'latitude' and 'longitude' query arguments are required")

    latitude = request.args.get("latitude")
    longitude = request.args.get("longitude")

    events = EventController.process_events_between_coordinates(latitude, longitude)
    return jsonify(events)
