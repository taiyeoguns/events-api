from flask import Blueprint

events_bp = Blueprint("events", __name__)

from events import urls  # noqa
