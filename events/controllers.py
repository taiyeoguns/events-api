from datetime import datetime

import geopy.distance
import requests
from flask import abort
from werkzeug.exceptions import NotFound

from config import Config
from events.services import APIService, DBService


class EventController:
    """Controller for processing events
    """

    @staticmethod
    def process_event(ip_address):
        """Process event from IP address

        Arguments:
            ip_address {str} -- IP address

        Raises:
            NotFound: Exception raised if data is not available

        Returns:
            str -- Dictionary of event details
        """
        try:
            # check if event with ip already exists
            _event = DBService.get_event_by_ip(ip_address)

            # if not available, geolocate and store in database
            if _event is None:
                api_service = APIService(Config.API_URL)

                ip_details = api_service.get_ip_details(ip_address)

                if ip_details is None:
                    raise NotFound(f"Details for IP '{ip_address}' not found")

                _event = DBService.add_event(ip_address, ip_details)

            return _event.to_mongo().to_dict()

        except (requests.exceptions.RequestException, ValueError) as e:
            abort(400, f"An error occurred: {e}")
        except NotFound as e:
            abort(404, e)
        except Exception as e:
            abort(500, e)
            raise

    @staticmethod
    def process_events_between_dates(from_date_str, to_date_str):
        """Process events between range of dates

        Arguments:
            from_date_str {str} -- 'from' date string (DD-MM-YYYY)
            to_date_str {str} -- 'to' date string (DD-MM-YYYY)

        Raises:
            NotFound: Exception raised if data is not available

        Returns:
            list -- List of dictionaries of events matching range
        """
        date_format = "%d-%m-%Y"

        try:
            # convert from string to datetime
            from_date = datetime.strptime(from_date_str, date_format)
            to_date = datetime.strptime(to_date_str, date_format)

            # get events between dates provided
            _events = DBService.get_events_between_dates(from_date, to_date)

            # if no events found for dates, return message to user
            if len(_events) < 1:
                raise NotFound

            return [_event.to_mongo().to_dict() for _event in _events]

        except (requests.exceptions.RequestException, ValueError) as e:
            abort(400, f"An error occurred: {e}")
        except NotFound:
            abort(404, "No events found for dates provided")
        except Exception as e:
            abort(500, e)
            raise

    @staticmethod
    def process_events_between_coordinates(latitude, longitude):
        """Process events within distance of coordinates provided

        Arguments:
            latitude {str} -- Latitude value of coordinate
            longitude {str} -- Longitude value of coordinate

        Raises:
            NotFound: Exception raised if data is not available
            NotFound: Exception raised if data is not available

        Returns:
            list -- List of dictionaries of events close to coordinates
        """

        latitude, longitude = float(latitude), float(longitude)

        try:
            # get events
            _events = DBService.get_all_events()

            # if no events found, return message to user
            if len(_events) < 1:
                raise NotFound

            origin = (latitude, longitude)

            events_within_range = [
                _event
                for _event in _events
                if _event_is_within_range(
                    origin,
                    (float(_event.location.latitude), float(_event.location.longitude)),
                    radius=500,
                )
            ]

            if len(events_within_range) < 1:
                raise NotFound

            return [_event.to_mongo().to_dict() for _event in events_within_range]

        except (requests.exceptions.RequestException, ValueError) as e:
            abort(400, f"An error occurred: {e}")
        except NotFound:
            abort(404, "No events available")
        except Exception as e:
            abort(500, e)
            raise


def _event_is_within_range(origin, destination, radius=20):
    """Helper function to filter events within range

    Arguments:
        origin {tuple} -- Coordinates for origin position
        destination {tuple} -- Coordinates for destination position

    Keyword Arguments:
        radius {int} -- Distance (in miles) to location (default: {20})

    Raises:
        TypeError: Exception raised for wrong type
        TypeError: Exception raised for wrong type

    Returns:
        bool -- True or False depending on if within radius
    """
    if not all(isinstance(arg, tuple) for arg in (origin, destination)):
        raise TypeError(f"'{origin}' and '{destination}' should be tuples")

    if not isinstance(radius, int):
        raise TypeError(f"'{radius}' should be integer")

    if geopy.distance.distance(origin, destination).miles > radius:
        return False

    return True
