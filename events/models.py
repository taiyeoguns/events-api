from datetime import datetime

from app.extensions import db


class Country(db.EmbeddedDocument):
    iso_code = db.StringField()
    name = db.StringField()


class Location(db.EmbeddedDocument):
    accuracy_radius = db.IntField()
    latitude = db.FloatField()
    longitude = db.FloatField()
    metro_code = db.IntField()
    time_zone = db.StringField()


class Event(db.Document):
    name = db.StringField(required=True)
    source_ip = db.StringField(required=True)
    added_at = db.DateTimeField(default=datetime.utcnow)
    country = db.EmbeddedDocumentField(Country)
    city = db.StringField()
    location = db.EmbeddedDocumentField(Location)
