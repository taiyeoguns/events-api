import ipaddress
import random

import requests
from mongoengine import ValidationError
from tenacity import (
    retry,
    retry_if_exception_message,
    retry_if_exception_type,
    wait_exponential,
)

from events.models import Country, Event, Location


class APIService:
    """Service to retrieve geolocation data from API
    """

    def __init__(self, location):
        self.location = location

    @retry(
        wait=wait_exponential(multiplier=1, min=4, max=10),
        retry=(
            retry_if_exception_type(requests.exceptions.HTTPError)
            & retry_if_exception_message("too many calls")
        ),
    )
    def get_ip_details(self, ip):
        """Gets geolocation data by IP address

        Waits and retries if API limit is reached

        Arguments:
            ip {str} -- IP address

        Returns:
            dict -- Dictionary of response from API if found
        """
        try:
            ip_addr = str(ipaddress.ip_address(ip))

            response = requests.get(f"{self.location}/info", params=dict(ip=ip_addr))
            response.raise_for_status()
        except ValueError as e:
            print(e)
            raise
        except requests.exceptions.HTTPError as e:
            print(e)
            if e.response.status_code == 404:
                return None
            raise
        except requests.exceptions.RequestException as e:
            print(e)
            raise

        return response.json()


class DBService:
    """Service to create/read/update/delete from database
    """

    @staticmethod
    def get_event_by_ip(ip_addr):
        """Get single event from database

        Arguments:
            ip_addr {str} -- IP address

        Returns:
            document -- Mongo Document object
        """
        return Event.objects(source_ip=ip_addr).exclude("id").first()

    @staticmethod
    def add_event(ip_addr, ip_details):
        """Add event to database

        Arguments:
            ip_addr {str} -- IP Address
            ip_details {dict} -- Dictionary of IP details from API

        Returns:
            document -- Mongo Document object added to database
        """
        try:
            new_event = Event(
                name=_get_random_name(), source_ip=ip_addr, city=ip_details["city"]
            )

            new_country = Country(**ip_details["country"])
            new_location = Location(**ip_details["location"])

            new_event.country = new_country
            new_event.location = new_location

            new_event.save()

            _event = Event.objects(id=new_event.id).exclude("id").first()
        except ValidationError as e:
            print(e)
            raise
        finally:
            return _event

    @staticmethod
    def get_events_between_dates(from_date, to_date):
        """Gets events from database within given date range

        Arguments:
            from_date {datetime} -- Date to search from
            to_date {datetime} -- Date to search to

        Returns:
            list -- List of Mongo Document objects
        """
        return Event.objects(added_at__gte=from_date, added_at__lt=to_date).exclude("id")

    @staticmethod
    def get_all_events():
        """Get all events from database

        Returns:
            list -- List of Mongo Document objects
        """
        return Event.objects.exclude("id")


def _get_random_name():
    """Generate random name for event

    Returns:
        str -- Name for event
    """
    _chars = "ABCDEFGH12345678"
    return "".join(random.choices(_chars, k=4))
