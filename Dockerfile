FROM python:3.7-alpine

# Create a directory for application
RUN mkdir -p usr/src/app

# Copy everything to app folder
COPY . /usr/src/app/

# Make app as working directory
WORKDIR /usr/src/app

# Install the Python libraries
RUN pip install --no-cache-dir pipenv
RUN pipenv install --dev --system

EXPOSE 5000

# Run the start script
CMD ["sh", "start.sh"]
