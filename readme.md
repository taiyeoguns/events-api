# Events - API

API for events.

Built with Python3 and Flask.

## Requirements

- Python 3.7
- Docker and Docker Compose
- Flask
- Mongodb
- Redis
- Documentation with OpenAPI 3

## Installation

### Clone Project

```sh
git clone https://gitlab.com/taiyeoguns/events-api.git
```

Change to cloned directory, e.g.

```sh
cd events-api
```

### Start application

With Docker and Docker Compose set up, run:

```sh
docker-compose up
```

Wait till setup is complete and all containers are started.

Thereafter, application should be available at `http://localhost:5000`

### Documentation

Documentation for the API should be available at: `http://localhost:5000/api/ui/`

#### Tests

When containers are running, execute tests with:

```sh
docker exec -it flask_container pytest -v
```
