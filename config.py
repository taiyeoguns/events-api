from os import getenv
from os.path import abspath, dirname

basedir = abspath(dirname(__file__))


class Config:
    """General configuration
    """

    SECRET_KEY = getenv("SECRET_KEY") or "s3cRe7-kE4"
    DEBUG = False

    # api
    API_URL = getenv("IP_API_BASE_URL")

    # mongo
    MONGODB_DB = getenv("MONGO_DATABASE")
    MONGODB_HOST = getenv("MONGO_HOST")
    MONGODB_USERNAME = getenv("MONGO_USERNAME")
    MONGODB_PASSWORD = getenv("MONGO_PASSWORD")

    # redis
    CACHE_TYPE = "redis"
    CACHE_REDIS_URL = getenv("REDIS_URL")
    CACHE_DEFAULT_TIMEOUT = 300


class DevelopmentConfig(Config):
    """Development configuration

    Arguments:
        Config {class} -- General configuration
    """

    DEBUG = True


class TestingConfig(Config):
    """Testing configuration

    Arguments:
        Config {class} -- General configuration
    """

    TESTING = True
